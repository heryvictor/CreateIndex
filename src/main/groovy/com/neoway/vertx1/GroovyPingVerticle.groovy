
package com.neoway.vertx1

import org.vertx.groovy.platform.Verticle


class GroovyPingVerticle extends Verticle {

    def start() {
        vertx.eventBus.registerHandler("ping-address") { message ->
            message.reply("pong!")
            container.logger.info("Sent back pong groovy!")
        }
    }

}

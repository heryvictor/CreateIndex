package com.neoway.vertx1

import org.vertx.groovy.platform.Verticle


class CreateIndex extends Verticle {
    def start() {

        //def mongoConf = container.getConfig()['mongo-persistor']

        def mongoRequest = [
//            'action': 'command',
//            'command': "{'createIndexes': 'users', indexes: [{key: {name: 1}, name: 'name_1'}]}"
            'action': 'command',
            'command': "{'listIndexes': 'users'}"
        ]

        def mongoConf = new HashMap()
        mongoConf.address = "mongodb-persistor"
        mongoConf.host = "core1"
        mongoConf.port = 27001
        mongoConf.pool_size = 10
        mongoConf.db_name = "db1"

        container.deployModule("io.vertx~mod-mongo-persistor~2.1.1-SNAPSHOT", mongoConf) { response ->
            container.logger.info(response)

            vertx.eventBus.send('mongodb-persistor', mongoRequest) { m ->
                println "I received a reply ${m.body}"
            }
        }
    }
}

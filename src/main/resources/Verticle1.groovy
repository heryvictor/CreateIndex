
vertx.eventBus.registerHandler("ping-address") { message ->
    container.logger.info("Sent back pong groovy!"+ message.body.message)
    message.reply("Pong!")
}

for ( i in 0..5 ) {
    def index = "" + i
    vertx.eventBus.send("ping-address", ['message': "Valor $i"]) { response ->
        container.logger.info(response.body)
    }
}

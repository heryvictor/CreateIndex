
def mongoConfig = container.getConfig()['mongo-persistor']

//print(mongoConfig)
def mongoRequest = [
    'action': 'save',
    'collection': 'users',
    'document': ['name': 'Hery1']
]

Map mongoConf = [
    "address": "mongodb-persistor",
    "host": "core1",
    "port": 27001,
    "pool_size": 10,
    "db_name": "db1"
]

println(container.getClass())

container.deployMoule("io.vertx~mod-mongo-persistor~2.1.1-SNAPSHOT", (Map)mongoConf, 1)

vertx.eventBus.send('mongodb-persistor', mongoRequest) { response ->
    container.logger.info(response.body)
}
